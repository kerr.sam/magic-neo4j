import requests
from pprint import pprint
import json

endpoint = 'https://api.scryfall.com'

response = json.loads(requests.get(endpoint + '/sets/ELD').content)

set_id = response['id']
cards_in_set = response['card_count']

eldraine_cards = []
i = 0

while len(eldraine_cards) < cards_in_set:
    i = i + 1
    print 'Pulling ID %d' % i
    dest = endpoint + ('/cards/eld/%d' % i)
    response = json.loads(requests.get(dest).content)
    if 'status' in response and response['status'] == 404:
        print 'Skipping ID %d' % i
        continue
    eldraine_cards.append(response)


with open('eldraine.pickle', 'w') as f:
    import pickle
    pickle.dump(eldraine_cards, f)
